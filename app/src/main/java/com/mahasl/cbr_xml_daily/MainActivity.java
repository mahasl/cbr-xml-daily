package com.mahasl.cbr_xml_daily;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    String path = "https://www.cbr-xml-daily.ru/daily_json.js";
    private List<Valute> valuteList = new ArrayList<>();
    private int valuteCount;
    int cellHeight = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        runValuteWorker();
    }

    private void runValuteWorker() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    getValuteData();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 30000);
    }

    protected void getValuteData() throws IOException, JSONException {

        valuteList.clear();

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(path).build();

        Response response = client.newCall(request).execute();
        String responseText = response.body().string();

        JSONObject jsonObject = new JSONObject(responseText);
        JSONArray valutesNames = ((JSONObject) jsonObject.get("Valute")).names();
        JSONObject valutes = jsonObject.getJSONObject("Valute");
        for (int i = 0; i < valutes.length(); i++) {
            JSONObject currObject = valutes.getJSONObject(valutesNames.getString(i));
            String charCode = currObject.getString("CharCode");
            String name = currObject.getString("Name");
            double value = currObject.getDouble("Value");
            value = Math.round(value * 100.0) / 100.0;
            valuteList.add(new Valute(charCode, name, value));
        }
        fillTable();
    }

    private void fillTable() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TableLayout tableLayout = findViewById(R.id.tableLayout);
                if (valuteCount > 0) {
                    tableLayout.removeViews(1, valuteCount);
                }
                for (Valute valute : valuteList) {
                    TableRow tableRow = new TableRow(MainActivity.this);

                    tableRow.addView(createTextView(valute.getName()));
                    tableRow.addView(createTextView(String.valueOf(valute.getValue())));
                    tableRow.addView(createTextView(valute.getCharCode()));

                    tableLayout.addView(tableRow);
                }
                valuteCount = valuteList.size();
            }
        });
    }

    TextView createTextView(String text) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, cellHeight, 1f);
        TextView result = new TextView(MainActivity.this);
        result.setText(text);
        result.setLayoutParams(params);
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        getValuteData();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        return true;
    }
}
