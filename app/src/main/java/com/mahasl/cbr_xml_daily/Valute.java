package com.mahasl.cbr_xml_daily;

class Valute {
    private String charCode;
    private String name;
    private double value;


    Valute(String charCode, String name, double value) {
        this.charCode = charCode;
        this.name = name;
        this.value = value;
    }

    String getName() {
        return name;
    }

    double getValue() {
        return value;
    }

    String getCharCode() {
        return charCode;
    }
}
